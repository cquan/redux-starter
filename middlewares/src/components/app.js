import React, { Component } from 'react';
import UserList from './userLists';

export default class App extends Component {
  render() {
    return (
        <div>
            <UserList />
        </div>
    );
  }
}
