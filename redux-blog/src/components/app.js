import React, { Component } from 'react';

export default class App extends Component {
  render() {
    return (
        <div>
            {this.props.children}
            {/*
                this line was added for showing EXPLICITLY where to render
                the component that are nested inside of this app in routes
            */}
        </div>
    );
  }
}
