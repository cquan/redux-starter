import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchPosts } from '../actions/index';
import { Link } from 'react-router';

class PostsIndex extends Component {

    //whenever this Component will be rendered for the FIRST time, this method
    //will be called. Remeber, it will only be called once, only the first time
    //it will be called not the times that this Component got re-rendered
    componentWillMount(){
        this.props.fetchPosts();
    }

    renderPosts() {
        return this.props.posts.map( (post)=>{
            return (
                <li className="list-group-item" key={post.id}>
                    <Link to={`posts/${post.id}`}>
                        <span className="pull-xs-right">{post.categories}</span>
                        <strong>{post.title}</strong>
                    </Link>
                </li>
            )
        });
    }

    render(){
        return (
            <div>
                <div className="text-xs-right">
                    {/*The link here will be rendered as a anchor in html*/}
                    <Link className="btn btn-primary" to="/posts/new">
                        Add a Post
                    </Link>
                </div>
                <ul className="list-group">
                    {this.renderPosts()}
                </ul>
            </div>
        );
    }

}

function mapStateToProps(state) {
    return {
        posts: state.posts.all
    }
}
export default connect(mapStateToProps, { fetchPosts })(PostsIndex);
//                              ↑ this is the shortcut for mapDispatchToProps function.
//by doing this, we can remove the mapDispatchToProps function completely and can still
//be able to access the action creators with `this.props.xxx`
