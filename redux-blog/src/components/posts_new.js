import React, { Component, PropTypes } from 'react';
import { reduxForm } from 'redux-form';
import { createPost } from '../actions/index';
import { Link } from 'react-router';
import _ from 'lodash';


const FIELDS = {
    title: {
        type: 'input',
        label: 'Title for Post'
    },
    categories: {
        type: 'input',
        label: 'Enter some categories for this post'
    },
    content: {
        type: 'textarea',
        label: 'Post Contents'
    }
};
//              ['title', 'categories', 'content']; //indicates different pieces of data the form gonna contain
//                ^^^       ^^^           ^^^ these form helpers are used in multiple places
//                so it might be better to pull it out from the reduxForm function and have a constant storing
//                the some of the configuration of the field


class PostsNew extends Component {
  constructor(props) {
        super(props);

        this.state = {

        };
    }


    //the static here is defining a static object as contextTypes which can be
    //accessed by PostsNew.context; Whenever an instance of PostNew is
    //created, react will interpret this object. It understands that we declares
    //some contextTypes and it will also know that we want to specifically access
    //a property called `router` on our context. React is then going to search
    //ALL of the component's parents untill it finds this router property which
    //will then be assigned to `this.context.router` inside of this component
    static contextTypes = {
        router: PropTypes.object
    }


    //this is a helper function which will be passed to handleSubmit function
    //notice the props argument passed in and since we refered to `this` in
    //this method, we need to bind(this) on where this function got passed in
    onSubmit(props){
        // the createPost method returns an action which is a promise.
        // Thus, the best place to re-direct to another location is after this
        // promise resolves so we need to chain this promise here
        this.props.createPost(props).then( ()=>{
            console.log("navigate to /");
            this.context.router.push('/');
        });

    }

    renderField(fieldConfig, field) {
        var fieldHelper = this.props.fields[field];

        return (
            <div key={field} className={`form-group ${fieldHelper.touched && fieldHelper.invalid ? 'has-danger' : ''}`}>
                <label>{fieldConfig.label}</label>
                <fieldConfig.type className="form-control" type="text" {...fieldHelper}/>
                <div className="text-help">
                    {fieldHelper.touched ? fieldHelper.error : ''}
                </div>
            </div>
        );
    }


    render() {
        // `handleSubmit` is the redux form helper injected into the props of
        // this component. Whenever the form trying to sumit itself, we call
        // this helper function
        const { handleSubmit } = this.props;

        //pulling out the configuration objects provided by redux form  for
        //each input of the form.
        const { fields: { title, categories, content } } = this.props;
        //              ^ this is the es6 syntax same as:
        //const title = this.props.fields.title;

        return (
            /*
                Optionly, we can pass in an action creator which will be called
                when the form is submitted and validated.
                This action creator will basically contain the value of the form's
                input field which we can then send it to backend for creating the
                new posts
                                 |                                              */
            <form onSubmit={handleSubmit(this.onSubmit.bind(this))}>
                <h3>Create a new post</h3>
                {_.map(FIELDS, this.renderField.bind(this))}
                <button className="btn btn-primary" type="submit">Submit</button>
                <Link className="btn btn-danger" to="/">Cancel</Link>
            </form>
        );
    }
}

function validate(values) {
    const errors = {};

    // for each filed in FIELDS array, work on each type and filed
    _.each(FIELDS, (type, field)=>{
        //          ^^    ^^^^ the key|index of each element
        //          || the `type` of each element
        if ( ! values[field] ) {
            errors[field] = `Enter a ${field}`;
        }
    });

    return errors;
}


//reduxForm is quite similar to the connect function from redux which can be
//used to inject action creators and map state to props. Here's a quick
//comparison between function `reduxForm` and `connect`:
//connect(                  mapStateToProps, mapDispatchToProps)(...)
//reduxForm( formConfigObj, mapStateToProps, mapDispatchToProps)(...)
export default reduxForm({
    form: 'PostNew', //name the form, it does NOT need to map our component here, just make sure it is unique
    fields: _.keys(FIELDS),
    //        ^^^ convert the keys of an object into an array with strings
    validate
}, null, { createPost })(PostsNew);
//          ^ bind the action creator to props

//With the form configuration, our redux state will be like:
//state: {
//  form: {
//      PostsNewForm: {
//          title: '...',      <== user input
//          categories: '...', <== user input
//          content: '...'     <== user input
//      }
//  }
//}
//Redux-form will capture the input of the input of the user and update the
//content of the corresponding property in our redux state.
//The reduxForm function here is working as the bridge between the redux
//state and the component state
