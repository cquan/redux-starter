//setting up rules for routing our application
import React from 'react';
import { Route, IndexRoute } from 'react-router';

import App from './components/app.js';
import PostsIndex from './components/posts_index';
import PostsNew from './components/posts_new';
import PostsShow from './components/posts_show.js';

//creating a temperary functino component to practise with routes
const Greeting = ()=>{
    return <div>Hey there!</div>
}
//exporting the defined route rules
export default (
    <Route path="/" component={App} >
        {/*
            The component marked as IndexRoute will show only if the
            parent route is selected. So here, when user accessing "/",
            the component `App` and `PostsIndex` will be shown.
        */}
        <IndexRoute component={PostsIndex} />
        <Route path="posts/new" component={PostsNew} />
        <Route path="posts/:id" component={PostsShow} />
        {/*                ^^^ this is a parameter
            when user trying to access routes like 'posts/5', react router
            will automatically interpret it, extract 5 as id and pass its
            value into the `PostsShow` component as a special property,
            `this.props.params.id`
        */}
    </Route>
);
