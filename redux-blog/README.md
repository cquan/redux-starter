# Redux Blog
This project is mainly used for practising with react router and redux form

## Thoughts on React router
When user clicks link, the thing that we care MOST is the **change** of the URL. After installing the `react-router`, the `History` is also automatically installed which is desinged to manage the *history of the browser* by watching the change of the URL and is able to updates over time.
After user clicking the url, the `History` package will take that URL and inform `React-Router` which will **updates** the components of the page depending on the URL.
Then the `React-Router` indicates the React to re-render the page.
