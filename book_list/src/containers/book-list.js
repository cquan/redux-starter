import React, { Component } from 'react';
import { connect } from 'react-redux';

//import the action creator
import { selectBook } from '../actions/index';
import { bindActionCreators } from 'redux';


//this Component will also works as a container that has a
//direct connection to a state managed by redux.
//Containers are also known as Smart Component as oppose to Dump Component
//and normally containers should be placed in a separate folder named `Containers`
class BookList extends Component {

    renderList(){
        return this.props.books.map( (book)=>{
            return (
                <li
                    key={book.title}
                    onClick={ ()=>{ this.props.selectBook(book); }}
                    className="list-group-item">
                    {book.title}
                </li>
            );
        });
    }

    render() {
    return (
        <ul className="list-group col-sm-4">
            {this.renderList()}
        </ul>
        );
    }
}

//take the application state as argument and maps required part to this.props
function mapStateToProps(state) {
    return {
        books: state.books
    }
}

//Anything returned from this function will end up as props on the BookList
//Container. In our case, we can explicitly call this.props.selectBook to refer
//to the selectBook we imported in the beginning
function mapDispatchToProps(dispatch) {
    // whenever selectBook is called, the result should be passed to
    // all of our reducers
    // The dispatch object receives all the actions and send them to
    // all of the other reducers
    return bindActionCreators( { selectBook: selectBook }, dispatch );
}

//Promote BookList from a Component to a Container - it needs to know about:
//  * how to map application state into its own props -- mapStateToProps
//  * how to map action creators into its own props -- mapDispatchToProps
export default connect(mapStateToProps, mapDispatchToProps)(BookList);

//Which component should be container?
//* MOST parent component that carries a PARTICULAR piece of state

//Note:
//  * whenever the Appliation state changes, our container will automatically
//      re-render itself
//  * whenever the Application state changes, the new state will be assinged to
//      mapStateToProps function and update the this.props
