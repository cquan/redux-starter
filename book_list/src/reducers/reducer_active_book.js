//the reducer function will get two argument normally
//  * action: the action dispatched from the action creator
//  * state: NOT application state but the state this reducer is responsible for
//                              set the default value of state to null
export default function(state = null, action) {
    switch (action.type) {
        case 'BOOK_SELECTED':
            return action.payload;
            //we can NOT alter state here, we can only return a fresh object.
    }
    return state
};


//Note: redux does NOT allow reducer to return undefined.
