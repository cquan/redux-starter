//a reduecer is a function that returns a piece of the application's state
export default function() {
    return [
        {title: 'Book 1', pages: 101},
        {title: 'Book 2', pages: 10},
        {title: 'Book 3', pages: 11},
        {title: 'Book 4', pages: 1}
    ]
};
