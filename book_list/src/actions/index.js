//this file should contain all  the actions that we made

export function selectBook(book) {
    //an action object should be returned here:
    //an object with a type property
    return {
        type: 'BOOK_SELECTED', //necessary
        payload: book //optional
    }
}
