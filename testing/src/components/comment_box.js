import React, { Component } from 'react';
import { connect } from 'react-redux';
// import all actions and store them into `actions` variable
import * as actions from '../actions';
import { bindActionCreators } from 'redux';

class CommentBox extends Component {
    constructor(props) {
        super(props);

        this.state = {
            comment: ''
        };
    }
    handleChange(e){
        this.setState({
            comment: e.target.value
        });
    }
    handleSubmit(e){
        e.preventDefault();
        this.props.saveComment(this.state.comment);
        this.setState({
            comment: ''
        });
    }

    render() {
        return (
            //it is normally helpful to let the top level element share the
            //same class name as the Component
            <form onSubmit={this.handleSubmit.bind(this)} className="comment-box">
                <h4>Add comment:</h4>
                <textarea
                    onChange={this.handleChange.bind(this)}
                    value={this.state.comment}
                />
                <div>
                    <button action='submit'>Submit Comment</button>
                </div>
            </form>
        );
    }
}

export default connect(null, actions)(CommentBox);
//                           ^^^^^^ by passing the actions here, all actions imported
//                           will be bond to `CommentBox` automatically and can be
//                           accessed by `this.props.ACTION_NAME`
