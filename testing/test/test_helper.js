import jsdom from 'jsdom';
import _$ from 'jquery';
//     ^^ this is kind of a junky jquery and we will define our own instance of normal jquery,
//     the dollar sign $.
import TestUtils from 'react-addons-test-utils';
import ReactDOM from 'react-dom';
import chai, { expect } from 'chai';
import React from 'react';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import reducers from '../src/reducers';
import chaiJquery from 'chai-jquery';

// GOAL 1: set up testing environment to run like a browser in the command line
// 1. jsdom: a javascript implementation of the DOM and HTML standards for use with Node.js
//
// Scope:
//   * in browser,  the global scope can be refered to as `window`
//   * in commandline, the global scope is referred to as `global`
// This create our simulated environment as browser which can be used at the commandline
global.document = jsdom.jsdom('<!doctype html><html><body></body></html>');
//     ^^^^^^ when running jquery, it assumes that the global `document` variable is available.
//     That's why we create it and we also need to create the global `window` variable:
global.window = global.document.defaultView;

const $ = _$(global.window);
//             ^^^^^ create our own useful jquery instance and bind it with the `window` we defined

// GOAL 2: build 'renderComponent' helper that should render a given react class
function renderComponent(ComponentClass, props, state) {
    //                    ^^^^^^^^^^     ^^^^    ^^^
    //ComponentClass: the class we created and we want an instance of it to return.
    //props:any props should be placed DIRECTLY onto the <ComponentClass />
    //state:any application level state that should be injected in redux store to allow
    //      our component to get data from redux store

    const componentInstance = TestUtils.renderIntoDocument(
        //wherever we use JSX, we need to import `React` library
        <Provider store={createStore(reducers, state)}>
            {/*             ^^^^                ^^^preloaded application state*/}
            {/*             ^^^^ with createStore, we need to pass reducers*/}
            <ComponentClass {...props} />
        </Provider>
    );

    // ReactDOM.findDOMNode produces the HTML references
    return $(ReactDOM.findDOMNode(componentInstance));
    //     ^ wrap it into jquery so that we can make use of good jquery matchers
}
// Render Redux Containers:
// To render components that connected to redux, such component is expected to
// be rendered underneath the `Provider` element, AKA, the child of the
// `Provider` component. Therefore, we need to wrap our component inside of a
// `Provider` and we need to pass the `Provider` a redux store as well

// GOAL 3: build helper for simulating events
//|-add simulate function to jquery and it can be called: $('div').simulate
$.fn.simulate = function(eventName, value) {
    if (value) {
        this.val(value);
        //   ^^^ a jquery method to set the value of the selected element
    }
    //                    |- pass in the eventname
    TestUtils.Simulate[eventName](this[0]);
    //                             ^^ this refers to the element that calls
    //this function. The [0] is effectively selecting the first element
    //selected by the jquery selector since $('div') might return an array
    //which contains multiple div elements
}


// GOAL 4: set up chai-jquery
chaiJquery(chai, chai.util, $);

export { renderComponent, expect };
