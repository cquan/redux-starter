/**
 * This is the test file for comments reducer
 * Whenever we test a reducer, we'll:
 * 1. test its DEFAULT case which is we pass an action and we know that this
 * reducer will not respond to it. Its purpose is just testing the initial
 * state of the reducer.
 * 2. then we will test every action we care about
 */

import { expect } from '../test_helper';
import commentReducer from '../../src/reducers/comments';
import { SAVE_COMMENT } from '../../src/actions/types';

describe("Comments Reducer", ()=>{
    it("handles action with unknow type", ()=>{
        expect(commentReducer(undefined, {})).to.eql([]);
        //                          ^^^ `eql` will do a deep comparison of the
        //                          target array for every element inside of it
        //                          while `equal` checks if the two object are
        //                          abosolute identical (reference). Therefore,
        //                          `equal` is often used with strings and numbers
    });

    it("SAVE_COMMENT", ()=>{
        const action= { type: SAVE_COMMENT, payload: 'new comment'};
        expect(commentReducer([], action)).to.eql(['new comment']);
    });
});
