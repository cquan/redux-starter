import { renderComponent, expect } from '../test_helper';
import CommentBox from '../../src/components/comment_box';

describe('CommentBox', ()=>{

    //define the component in advance and resign it in beforeEach block
    let component;

    // runs before any of each component
    beforeEach( ()=>{
        //                 --renderComponent actually returns an jquery object that
        //                 | contains our component so that we can use any method
        //                 | provided by jquery. And jquery can help us to make
        //                 | assertions about the html content our component produce
        component = renderComponent(CommentBox);
    });

    it('has a text area', ()=>{
        //here we need to use chai-jquery library for testing
        expect(component.find('textarea')).to.exist;
        //               ^^^^ jquery to find the text area
    });
    it('has a button', ()=>{
        expect(component.find('button')).to.exist;
    });
    it('has the correct class', ()=>{
        //here we need to use chai-jquery library for testing
        expect(component).to.have.class('comment-box');
    });

    //for `it` statement that are very close related, we can nest ANOTHER `describe`
    //block here
    describe('entering some text', ()=>{
        //beforeEach statement is scope insided of the current describe and for
        //nested describe blocks, the OUTERMOST beforeEach will run FIRST and goes
        //all the way down to the current beforeEach statement.
        beforeEach( ()=>{
            // here we need to pre-populate the textarea
            // insert text by simulating a change event
            component.find('textarea').simulate('change', 'new comment');
        });
        it('shows that text in the textarea', ()=>{
            expect(component.find('textarea')).to.have.value('new comment');
        });

        it('when submitted, clears the input', ()=>{
            component.simulate('submit');
            expect(component.find('textarea')).to.have.value('');
        });
    });
});
