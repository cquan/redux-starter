import { expect } from '../test_helper';
import { SAVE_COMMENT } from '../../src/actions/types';
import { saveComment } from '../../src/actions';

describe("actions", ()=>{
    // when testing actions, the tests tend to be more low level.E.g. action
    // type and payload
    describe("saveComment", ()=>{

        it('has the correct type', ()=>{
            const action = saveComment();
            expect(action.type).to.equal(SAVE_COMMENT);
        });

        it('has the correct payload', ()=>{
            const action = saveComment('new comment');
            expect(action.payload).to.equal('new comment');
        });
    });
});
