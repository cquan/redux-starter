//whenever we are dealing with a React Component, we need to import it
import React, {Component} from 'react';

class SearchBar extends Component{

    //constructor is called whenever this component is initialized
    constructor(props) {
        //calling the parent's method here with super
        super(props);
        /*State:
          * state is a plain javascript to record in React to use events
          * Each instance of class based component has its own state object
          * whenever the component's state changes, the component immediately got re-rendered
          * also forces its children to re-render as well
         */
        //the state is initialized here and this.state can ONLY be used to initialize the
        //state in the constructor and when we want to update the state, use this.setState()
        this.state = {
            term: ''
        };
    }

    onInputChange(term){
        this.setState({term});
        this.props.onSearchTermChange(term);
    }


    //define the render method
    render(){
        //create an input element and pass the property onChange with
        //  this value to it
        return (
            <div className="search-bar">
                {/*convert input as controlled component*/}
                <input
                    value={this.state.term}
                    onChange={ event => this.onInputChange(event.target.value)} />
            </div>
        );
    }

}

export default SearchBar;
