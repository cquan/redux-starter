//this can be a functional component
import React from 'react';
import VideoListItem from './video_list_item';

const VideoList = (props)=>{
    const videoItems = props.videos.map( (video)=>{
        //when creating an array of elements, the best practise is always giving each
        //element an unique so that when doing re-rendering, react can locate the
        //element that need updating in the quickest way
        return (
            <VideoListItem
                onVideoSelect={props.onVideoSelect}
                key={video.etag}
                video={video} />
        );
    });
    return (
        <ul className="col-md-4 list-group">
            {videoItems}
        </ul>
    )
}

export default VideoList;
