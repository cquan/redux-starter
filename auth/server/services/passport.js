/**
 * Config file for Passport.js which is used for authenticate the user
 * Passort is more like an ecosystem which consists of different strategies and
 * in passport, a strategy is a method to `authenticate` a user. So to work with
 * JWT authentication, we also need the package `passport-jwt`.
 * So our main job here is to setup the config/strategies for authenticating jwt
 * tokens using passport
 */
const passport = require('passport');
const User = require('../models/user');
const config = require('../config');
const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;
const LocalStrategy = require('passport-local');

//create local strategy for user sign in
const localOptions = { usernameField: 'email'};
//                      ^^^^           ^^^
//for username, looking for the `email` property in the REQUEST.

const localLogin = new LocalStrategy(localOptions, function(email, password, done) {
    User.findOne({ email: email }, function(err, user) {
        if (err) { return done(err, false); }
        if (!user) { return done(null, false); }

        //compare the extracted password with the store password. Here we can
        //not do the plain string comparison since the password is hased with
        //the salt and stored in the database. Instead, we should:
        //1. get the salt from the database
        //2. hashed the salt with the extracted password
        //3. compare the hashed result with the stored password
        user.comparePassword(password, function(err, isMatch) {
            if (err) {
                return done(err, false);
            }
            if (!isMatch) {
                return done(null, false);
            }
            return done(null, user);
            //                ^^^ the found user instance will be signed to `req.user`
            //                so that it can be accessed in router callback
        });
    });
});

//setup options for JWT strategy
const jwtOptions = {
    //tell where to find the jwt token from the request
    jwtFromRequest: ExtractJwt.fromHeader('authorization'),
    secretOrKey: config.secret //secret to decode the token
};

//craete JWT strategy
//the callback function get called when a user is trying to authenticate using
//this strategy
const jwtLogin = new JwtStrategy(jwtOptions, function(payload, done) {
    //                                                  ^^^    ^^^
    //payload: decoded jwt token
    //done: a callback function to determine when this authentication is sucessful or not

    User.findById(payload.sub, function(err, user) {
        if (err) {
            return done(err, false);
            //                ^^^ meanning no user found
        }
        if (user) {
            done(null, user); //return user to the `done` function
        }else{
            //while the finding process is OK, still no user is found
            done(null, false);
        }
    });
});

//tell passport to use this strategy
passport.use(jwtLogin);
passport.use(localLogin);
