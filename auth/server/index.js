/**
 * Main starting point of the application
 *
 */
var express = require('express');

const http = require('http');

const bodyParser = require('body-parser');

const morgan = require('morgan');

const app = express();

const router = require('./router');

const cors = require('cors');

//set up Mongoose
const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/auth');
//                                          ^^ create a db named auth in mongodb

// App setup -- basically using all kinds of middlewares in express
app.use(morgan('combined')); //logging requests of http requests

app.use(cors());
//      ^^^^middleware that is designed to handle cors and inform the server to
//      bypass this CORS protection and we can also pass options into the `cors()`
//      function to only allow CORS requests from some PARTICULAR domain

app.use(bodyParser.json({ type: '*/*'}));
//      ^^^^        ^^^   ^^     ^^^
//      parse incoming requests into json no matter what the request type is

router(app); //setup routing

// Server Setup
const port = process.env.PORT || 3090;
const server = http.createServer(app);
server.listen(port, ()=>{
    console.log(`server started on ${port}`);
});
