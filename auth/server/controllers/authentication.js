/**
 * The Authentication controller which specifically desinged to handle the
 * authentication logic when receiving such request
 *
 */
const User = require('../models/user.js');
const jwt = require('jwt-simple');
const config = require('../config');

function tokenForUser(user) {
    const timestamp = new Date().getTime();
    return jwt.encode({ sub: user.id, iat: timestamp }, config.secret);
    //                  ^^^           ^^^ issued at time
    //                  ^^^ short for subject meaning what this token is about
}

exports.signin = function(req, res, next) {
    // user has already had their email and password authed
    // just need to give them a token
    res.send({ token: tokenForUser(req.user) });
}

exports.signup = function(req, res, next) {
    const email = req.body.email;
    const password = req.body.password;

    if (!email || !password) {
        return res.status(422).send({ error: 'You must provide Email and Password!' });
    }

    // see if a user with the given email exists
    User.findOne({ email: email }, function(err, existingUser) {
        if (err) { return next(err); }
        // if a user with email does exist, return an error
        if (existingUser) {
            return res.status(422).send({ error: 'Email is in use!' });//unprocessable property
        }
        // if a user with a email does NOT exist, crate and save user record
        // |-create the user
        const user = new User({
            email: email,
            password: password
        });

        //actually save the user to the database
        user.save( function(err) {
            if (err) {
                return next(err);
            }
            // respond to request indicating the user was created
            res.json({ token: tokenForUser(user) });
        });
    });
}
