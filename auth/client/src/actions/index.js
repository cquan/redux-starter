import axios from 'axios';
import { browserHistory } from 'react-router';
import {
    AUTH_USER,
    UNAUTH_USER,
    AUTH_ERROR,
    FETCH_MESSAGE
} from './types';


//the url to make ajax request to
const ROOT_URL = 'http://localhost:3090';

export function signinUser({ email, password }) {
    return function(dispatch) {
        // Submit email/password to the server
        // axios.post will return a Promise
        axios.post( `${ROOT_URL}/signin`, { email, password })
            .then( (response)=>{
                //If request is good...
                //  - Update state to indicate user is authenticated
                dispatch({ type: AUTH_USER });
                //  - save the JWT token to local storage
                //    `localStorage` is an object available on `window` scope
                localStorage.setItem('token', response.data.token);
                //  - redirect to feature page
                browserHistory.push('/feature');
            })
            .catch( ()=>{
                //if request is bad...
                // - show an error to the usre
                dispatch(authError('Bad Login Info!'));
            });
    }
}


export function signupUser({ email, password }) {
    return function(dispatch) {
        axios.post(`${ROOT_URL}/signup`, { email, password })
            .then( (response)=>{
                dispatch({ type: AUTH_USER });
                localStorage.setItem('token', response.data.token);
                browserHistory.push('/feature');
            })
            .catch( response => dispatch(authError(response.data.error)));
    }
}

export function authError(error){
    return {
        type: AUTH_ERROR,
        payload: error
    }
}

export function signoutUser() {
    //remove the token from the localStorage
    localStorage.removeItem('token');

    return { type: UNAUTH_USER };
}

//make authenticated API request
export function fetchMessage() {
    return function(dispatch) {
        axios.get(ROOT_URL, {
            //pass in some options headers
            headers: {
                authorization: localStorage.getItem('token')
            }
        }).then( (response)=>{
            dispatch({
                type: FETCH_MESSAGE,
                payload: response.data.message
            });
        });
    }
}
