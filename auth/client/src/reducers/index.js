import { combineReducers } from 'redux';
import { reducer as form } from 'redux-form';
import authReducer from './auth_reducer';

const rootReducer = combineReducers({
    form, // this is same as `form: form` by es6 syntax
    auth: authReducer
});

export default rootReducer;
