import React, { Component } from 'react';
import { reduxForm } from 'redux-form';
import * as actions from '../../actions';

class SignUp extends Component {
    constructor(props) {
        super(props);

        this.state = {

        };
    }

    //this function get called when user trying to submit the form.
    //ReduxForm will check if all fields are valid and this function will
    //only get called if ALL fields are valid.
    handleFormSubmit(formProps){
        // Call action creator to sign up the user
        this.props.signupUser(formProps);
    }

    renderAlert(){
        if (this.props.errorMessage) {
            return(
                <div className="alert alert-danger">
                    <strong>Oops!</strong>{this.props.errorMessage}
                </div>
            )
        }
    }

    render() {
        const { handleSubmit, fields: { email, password, passwordConfirm }} = this.props;
        return (
            <form onSubmit={ handleSubmit(this.handleFormSubmit.bind(this)) }>
                <fieldset className="form-group">
                    <label htmlFor="">Email:</label>
                    <input className="form-control" {...email} />
                    {email.touched && email.error && <div className="error">{email.error}</div>}
                </fieldset>
                <fieldset className="form-group">
                    <label htmlFor="">Password:</label>
                    <input className="form-control" type="password" {...password} />
                    {password.touched && password.error && <div className="error">{password.error}</div>}
                    {/*the above code: if all three expressions are true, the last one get returned*/}
                </fieldset>
                <fieldset className="form-group">
                    <label htmlFor="">Confirm Password</label>
                    <input className="form-control" type="password" {...passwordConfirm} />
                    {passwordConfirm.touched && passwordConfirm.error && <div className="error">{passwordConfirm.error}</div>}
                </fieldset>
                {this.renderAlert()}
                <button className="btn btn-primary" type="submit">Submit!</button>
            </form>
        );
    }
}


function validate(formProps) {
    const errors = {};

    if (!formProps.email) {
        errors.email = 'Please enter an email!';
    }
    if (!formProps.password) {
        errors.password = 'Please enter a password!';
    }
    if (!formProps.passwordConfirm) {
        errors.passwordConfirm = 'Please enter a password confirmation!';
    }

    if ( formProps.password !== formProps.passwordConfirm ) {
        errors.password = 'Password must match!';
    }

    return errors;
}

function mapStateToProps(state) {
    return {
        errorMessage: state.auth.error
    };
}


export default reduxForm({
    form: 'signup',
    fields: ['email', 'password', 'passwordConfirm'],
    validate: validate // tell ReduxForm to use this validation function
}, mapStateToProps, actions)(SignUp);
