import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../../actions';

class Signout extends Component {
    constructor(props) {
        super(props);

        this.state = {

        };
    }

    componentWillMount(){
        console.log("signout user");
        this.props.signoutUser();
    }

    render() {
        return (
            <div>
                Sorry to see you go....
            </div>
        );
    }
}

export default connect(null, actions)(Signout);
