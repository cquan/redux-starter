/**
 * Higher Order Component for adding authentication functions
 * Basically we are defining a function that wraps a target component so that
 * 1. we can added additionaly functionalities onto this component
 * 2. we can also pass properties donw to the wrapped component
 *
 * This can also work as a skeleton for any HOC!!
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';

export default function(ComposedComponent) {
    //    |- this is a Higher Order Component
    class Authentication extends Component {
        //this contextTypes object is an class level object and can be accessed by
        //  `Authentication.contextTypes` directly ( prefixing the `static` keyword)
        static contextTypes = {
            // defined only to access router for redirecting it can be accessed by:
            //  this.context.router
            router: React.PropTypes.object
        }

        //this method will run everytime this component is rendered
        //note: when the user is already on this page, this function might not be called
        //as expected
        componentWillMount(){
            if ( !this.props.authenticated) {
                this.context.router.push('/');
            }
        }

        //this function is called when it is passed with new props
        componentWillUpdate(nextProps){
            if (!nextProps.authenticated) {
                this.context.router.push('/');
            }
        }

        render(){
            console.log(this.props.authenticated);
            return <ComposedComponent {...this.props} />;
            //                         ^^^^^ forward the properties passed to the `componsed component`
            //                         also to the target component as well
        }
    }

    function mapStateToProps(state) {
        return { authenticated: state.auth.authenticated }
    }

    return connect(mapStateToProps)(Authentication);
    //     ^^^^ connect here is also an HOC. Therefore, we can nest any number of HOCs together as we want
};

