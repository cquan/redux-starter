import React, { Component } from 'react';
//since we want header to be shown always, the App component is actually a good
//place to hold our header
import Header from './header.js';

export default class App extends Component {
  render() {
    return (
        <div>
            <Header />
            {this.props.children}
        </div>
    );
  }
}
