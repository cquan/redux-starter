# Client side auth

## Application state
```javascript
{
    auth: {
        authenticated: BOOLEAN, //whether or not the user is currently signed in
        error: STRING // auth related errors to be shown on uer's screen
    }
}
```
As for the `JWT token`, we don't really need to store it inside of our
application state (we need it stored somewhere, but not in our state).


##CROS
When user on a website that tries to make an AJAX requests to a
domain/subdomain to a different domain. The browser can reject such request.
The entire purpose of CROS is just to protect users in a browser environment.

A common fix for  this is to edit on the server side and tell the server to
allow requests from anywhere and bypass CROS.

## Where to save the JWT token -- LocalStorage
`LocalStorage` can be basically considered as *local hard drive access* on
user's computer and it is not shared on multipse devices of the same user;
it is just for one singler browser and it is NOT shared between different
domains.
This way brings to benefits:
1. when a user makes a request that needs to be authenticated, we can easily
find and attache the JWT token.
2. when user revisits our app, JWT is still available -- no need to login again

## Form validation using ReduxForm
ReduxForm uses a function, `validate()` to check if the inputs from users are
validate or not. For example, we have a form for user to input:
```
Email:
Password:
Password Confirm:
```
When any value of the three fields is changing, or the user hit the `submit` button,
an object that contains all the input values we be sent to the `validate` method:
```
{
    email: "test@example.com",
    password: "abcde",
    password Confirm: "abcde"
}
```
And inside of that method, another new object, `error`,  will be created which
contains the same keys but the contents each key has is the error message.
```
error: {
    email: "xxxx",
    password: "xxxx",
    password Confirm: "xxxx"
}
```
Therefore, in the end of this `validate` function, this error will be returned and
for keys that ARE validate, its correpsonding error value will be `null`; for keys
that are NOT validate, its correpsonding error will be shown.

