import React from 'react';
import { GoogleMapLoader, GoogleMap } from 'react-google-maps';

export default (props)=>{
    return (
        <GoogleMapLoader
            containerElement={ <div style={{ height: '100%' }} />}
            googleMapElement={
                <GoogleMap defaultZoom={12}
                    defaultCenter={{lat: props.lat, lng: props.lon}}
                />
            }
        />
    );
};

//Passing style directly on the component:
//  * this work same as styling html element with css
//  * the structure is like this:
//      style =  {              <=== javascript expression indicator
//          { height : '100%' } <=== here's the styling element (object) we pass in
//      }                       <=== javascript expression indicator

