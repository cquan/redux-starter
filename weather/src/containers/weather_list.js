import React, { Component } from 'react';
import { connect } from 'react-redux';
import Chart from '../components/chart.js';
import MapBox from '../components/google_map.js';

class WeatherList extends Component {
  constructor(props) {
        super(props);

        this.state = {

        };
    }

    renderWeather(cityData){
        const name = cityData.city.name;
        //extract temperature from the list
        const temps = cityData.list.map( (weather)=>{
            return weather.main.temp
        });
        const pressures = cityData.list.map( (weather)=>{
            return weather.main.pressure
        });
        const humidities = cityData.list.map( (weather)=>{
            return weather.main.humidity
        });

        const { lat, lon } = cityData.city.coord;

        return (
            <tr key={name}>
                <td><MapBox lat={lat} lon={lon} /></td>
                <td><Chart data={temps} color="orange" units="K"/></td>
                <td><Chart data={pressures} color="green" units="hPa"/></td>
                <td><Chart data={humidities} color="black" units="%"/></td>
            </tr>
        );
    }

    render() {
    return (
        <table className="table table-hover">
            <thead>
                <tr>
                    <th>City</th>
                    <th>Temperature (K)</th>
                    <th>Pressure (hPa)</th>
                    <th>Humidity (%)</th>
                </tr>
            </thead>
            <tbody>
                {this.props.weather.map(this.renderWeather)}
            </tbody>
        </table>
        );
    }
}

//                       same as const weather = state.weather
function mapStateToProps({ weather }) {
    return { weather };
    //same as { weather:weather }
}

export default connect(mapStateToProps)(WeatherList);
