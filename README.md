# My React Code Base
This REPO is mainly used for holding some of the practises with React related techniques including React basics, redux, routing with react-router, dealing with AJAX request using axios and etc.

## Video Player
This video player is kind of the youtube wrapper where user can search the youtube videos by inputing in the search bar and the APP will instantly return the searched results back to the user to view.
In this project, the main goal is practising basics of React including:

* basic react component structure
* functional component vs class component
* handling events
* passing data between components
* handling `null` props
* throttling the user input
* Bootstrap styling

### Screenshot:
![Selection_024.png](https://bitbucket.org/repo/ajboz8/images/3157409023-Selection_024.png)

## Book List
This project is a simple implementation of React and Redux including a deep dive between component and container, the design and implementation of action creator and reducer.


## Weather
This project explores explores some more advance coorperations between React and redux including:

* custom form submission
* middleware in redux
* get weather information with AJAX request
* show graphs using sparklines
* show the city map using google map api

### Screenshot
![Selection_025.png](https://bitbucket.org/repo/ajboz8/images/2742468416-Selection_025.png)

## Redux Blog
This project creates an App providing blog services by sending requests using RestFUL APIs to a backend hosted on Heroku. In this project, the usage and tricks of `react router` and `redux form` have been explored including:

* route defining and re-directing
* the usage of context types
* life cycle component of React components
* form submission and validation using redux form

### Screenshot:
Posts list:
![Selection_026.png](https://bitbucket.org/repo/ajboz8/images/3331310067-Selection_026.png)
Post details:
![Selection_027.png](https://bitbucket.org/repo/ajboz8/images/3345497419-Selection_027.png)
Add new posts:
![Selection_028.png](https://bitbucket.org/repo/ajboz8/images/3944240478-Selection_028.png)
