# Higher Order Component

Normal Component + High Order Component = Component with additional functionality or data

Higher Order Component is convinient for providing common properties and features for multiple components.

## Redux: `connect` and `Provider`
`connect` is a HOC that is made to communicate with the TOP level `Provider`.
`Provider` as in src/index.js holds the redux store and watch the redux store
directly. And whenever the store changes, `Provider` will indicate all connected
`components` about the changes and new state.

## HOC discussion
* The good thing about HOC is that they all somehow follow a same order => the code will look generally similar
